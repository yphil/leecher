# leecher.pl

**Leecher** lets you download all the files linked from a particular web page, allowing a filtering on the file extension.

Typically, to get aaaall the files when you don't want to be bugged clicking 128 download links.

## USAGE

    
```
#!sh

leecher.pl [URI] [PATH] [FILE_EXT]
```


Where `URI` is the address of a web page, `PATH` a directory where you
want to save the files, and `FILE_EXT` the extension of the files you
want to leech. You will then get a visual output of the file download
progress. Sit back, relax, and watch the pixels move.

## EXAMPLE

    
```
#!sh

⚡ leecher.pl http://www.boston.com/bigpicture/2013/08/creatures_great_and_small.html ~/Music/IndusDI/plop jpg

    ### Found :

    (163)
    big_picture_header.jpg
    bp1.jpg
    (...)
    bp8.jpg
    bp9.jpg

    ### DL those files in [/home/px/Music/IndusDI/plop]? (Y/n)

    ### [/home/px/Music/IndusDI/plop] does not exist, create it? (Y/n)

    big_picture_header.jpg
    ######################################################################## 100.0%

    bp1.jpg
    ######################################################################## 100.0%
```

(and so on)


## FEATURES

-   So yes, the `URI` can contain a lot of nasty stuff (the things ppl put in filenames, sheesh) the script will try to take care of this.
-   If the directory does not exist, you will be asked to create it.
-   All non-existing dirs leading to the directory will be created.
-   For obvious reasons, if your directory (`PATH`) name contains spaces (because you too are an idiot) you'll have to enclose the it in quotes of sorts.
-   If for some reason you interrupt the script and re-launch it with the same `PATH` (or if `URI` contains several links to the same file) **Leecher** will take care of not downloading the same file twice. All hail CURL !

## LICENSE

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.